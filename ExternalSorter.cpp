#include <cstdlib>
#include <iostream>
#include <sstream>
#include "ExternalSorter.h"

using std::ios_base;
using std::ifstream;
using std::stringstream;
using std::cout;
using std::endl;

template<typename T>
bool ExternalSorter::compLT(T x, T y) {
    return x < y;
}

template<typename T>
bool ExternalSorter::compGT(T x, T y) {
    return x > y;
}

void ExternalSorter::deleteTmpFiles() {
    for (int i = 0; i < file.size(); i++) {
        if (file[i]->f.is_open()) file[i]->f.close();
        remove(file[i]->name.c_str());
        delete file[i];
    }
    file.clear();
}

void ExternalSorter::termTmpFiles(int rc) {
    /* cleanup files */
    remove(ofName.c_str());
    if (rc == 0) {
        int fileT;

        /* file[T] contains results */
        fileT = nTmpFiles - 1;
        file[fileT]->f.close();
        if (rename(file[fileT]->name.c_str(), ofName.c_str())) {
            perror("io1");
            deleteTmpFiles();
            exit(1);
        }
    }
    deleteTmpFiles();
}

void ExternalSorter::cleanExit(int rc) {
    /* cleanup tmp files and exit */
    termTmpFiles(rc);
    exit(rc);
}

void ExternalSorter::initTmpFiles() {
    /* initialize merge files */
    if (nTmpFiles < 3) nTmpFiles = 3;
    file.resize(nTmpFiles);

    vector<TmpFileTag*> fileInfo(nTmpFiles);
    for (auto i = 0; i < nTmpFiles; i++) {
        fileInfo[i] = new TmpFileTag();
    }

    stringstream ss;
    for (auto i = 0; i < nTmpFiles; i++) {
        file[i] = fileInfo[i];

        ss.str("");
        ss << "_sort" << i << ".dat";
        file[i]->name = ss.str();

        file[i]->f.open(file[i]->name, ios_base::out | ios_base::in | ios_base::trunc | ios_base::binary);
        if (file[i]->f.fail()) {
            perror("io2");
            cleanExit(1);
        }
    }
}

RecType* ExternalSorter::readRec() {
    struct INodeTag {   /* internal node */
        struct INodeTag *parent;/* parent of internal node */
        struct ENodeTag *loser; /* external loser */
    };

    struct ENodeTag {   /* external node */
        struct INodeTag *parent;/* parent of external node */
        RecType rec;            /* input record */
        int run;                /* run number */
        bool valid;             /* input record is valid */
    };

    struct NodeTag {
        INodeTag i;            /* internal node */
        ENodeTag e;            /* external node */
    };

    static vector<NodeTag> node;/* array of selection tree nodes */
    static ENodeTag *win = nullptr;      /* new winner */
    static ifstream input;      /* input file */
    static bool eof = false;    /* true if end-of-file, input */
    static int maxRun = 0;          /* maximum run number */
    static int curRun = 0;          /* current run number */
    static INodeTag *p = nullptr;        /* pointer to internal nodes */
    static bool lastKeyValid = false;   /* true if lastKey is valid */
    static keyType lastKey = 0;     /* last key written */

    /* read next record using replacement selection */

    /* check for first call */
    if (node.empty()) {
        int i;

        if (nNodes < 2) nNodes = 2;
        node.resize(nNodes);
        for (i = 0; i < nNodes; i++) {
            node[i].i.loser = &node[i].e;
            node[i].i.parent = &node[i/2].i;
            node[i].e.parent = &node[(nNodes + i)/2].i;
            node[i].e.run = 0;
            node[i].e.valid = false;
        }
        win = &node[0].e;
        lastKeyValid = false;

        input.open(ifName, ios_base::in | ios_base::binary);
        if (input.fail()) {
            cout << "error: file " << ifName << ", unable to open" << endl;
            cleanExit(1);
        }
    }

    while (1) {
        /* replace previous winner with new record */
        if (!eof) {
            input.read((char*)&win->rec, sizeof(RecType));
            if (!input.fail()) {
                if ((!lastKeyValid || compLT(win->rec.key, lastKey))
                    && (++win->run > maxRun))
                    maxRun = win->run;
                win->valid = true;
            } else if (input.eof()) {
                input.close();
                eof = true;
                win->valid = false;
                win->run = maxRun + 1;
            } else {
                perror("io4");
                cleanExit(1);
            }
        } else {
            win->valid = false;
            win->run = maxRun + 1;
        }

        /* adjust loser and winner pointers */
        p = win->parent;
        do {
            bool swap;
            swap = false;
            if (p->loser->run < win->run) {
                swap = true;
            } else if (p->loser->run == win->run) {
                if (p->loser->valid && win->valid) {
                    if (compLT(p->loser->rec.key, win->rec.key))
                        swap = true;
                } else {
                    swap = true;
                }
            }
            if (swap) {
                /* p should be winner */
                ENodeTag *t;

                t = p->loser;
                p->loser = win;
                win = t;
            }
            p = p->parent;
        } while (p != &node[0].i);

        /* end of run? */
        if (win->run != curRun) {
            /* win->run = curRun + 1 */
            if (win->run > maxRun) {
                /* end of output */
                node.clear();
                return NULL;
            }
            curRun = win->run;
        }

        /* output top of tree */
        if (win->run) {
            lastKey = win->rec.key;
            lastKeyValid = true;
            return &win->rec;
        }
    }
}

void ExternalSorter::makeRuns() {
    RecType *win;       /* winner */
    int fileT;          /* last file */
    int fileP;          /* next to last file */

    /* Make initial runs using replacement selection.
     * Runs are written using a Fibonacci distintbution.
     */

    /* initialize file structures */
    fileT = nTmpFiles - 1;
    fileP = fileT - 1;
    for (auto j = 0; j < fileT; j++) {
        file[j]->fib = 1;
        file[j]->dummy = 1;
    }
    file[fileT]->fib = 0;
    file[fileT]->dummy = 0;

    level = 1;

    win = readRec();
    while (win) {
        bool anyrun;

        anyrun = false;
        for (auto j = 0; win && j <= fileP; j++) {
            bool run;

            run = false;
            if (file[j]->valid) {
                if (!compLT(win->key, file[j]->rec.key)) {
                    /* append to an existing run */
                    run = true;
                } else if (file[j]->dummy) {
                    /* start a new run */
                    file[j]->dummy--;
                    run = true;
                }
            } else {
                /* first run in file */
                file[j]->dummy--;
                run = true;
            }

            if (run) {
                anyrun = true;

                /* flush run */
                while(1) {
                    file[j]->f.write((char*)win, sizeof(RecType));
                    if (file[j]->f.fail()) {
                        perror("io3");
                        cleanExit(1);
                    }
                    file[j]->rec.key = win->key;
                    file[j]->valid = true;
                    if ((win = readRec()) == NULL) break;
                    if (compLT(win->key, file[j]->rec.key)) break;
                }
            }
        }

        /* if no room for runs, up a level */
        if (!anyrun) {
            int t;
            level++;
            t = file[0]->fib;
            for (auto jj = 0; jj <= fileP; jj++) {
                file[jj]->dummy = t + file[jj+1]->fib - file[jj]->fib;
                file[jj]->fib = t + file[jj+1]->fib;
            }
        }
    }
}

void ExternalSorter::rewindFile(int j) {
    /* rewind file[j] and read in first record */
    file[j]->eor = false;
    file[j]->eof = false;
    file[j]->f.seekg(0);
    file[j]->f.seekp(0);
    file[j]->f.read((char*)&file[j]->rec, sizeof(RecType));
    if (file[j]->f.fail()) {
        if (file[j]->f.eof()) {
            file[j]->eor = true;
            file[j]->eof = true;
        } else {
            perror("io5");
            cleanExit(1);
        }
    }
}

void ExternalSorter::mergeSort() {
    int fileT;
    int fileP;
    TmpFileTag* tfile;

    /* polyphase merge sort */

    fileT = nTmpFiles - 1;
    fileP = fileT - 1;

    /* prime the files */
    for (auto j = 0; j < fileT; j++) {
        rewindFile(j);
    }

    /* each pass through loop merges one run */
    while (level) {
        while(1) {
            bool allDummies;
            bool anyRuns;

            /* scan for runs */
            allDummies = true;
            anyRuns = false;
            for (auto j = 0; j <= fileP; j++) {
                if (!file[j]->dummy) {
                    allDummies = false;
                    if (!file[j]->eof) anyRuns = true;
                }
            }

            if (anyRuns) {
                int k;
                keyType lastKey;

                /* merge 1 run file[0]->.file[P] --> file[T] */

                while(1) {
                    /* each pass thru loop writes 1 record to file[fileT] */

                    /* find smallest key */
                    k = -1;
                    for (auto j = 0; j <= fileP; j++) {
                        if (file[j]->eor) continue;
                        if (file[j]->dummy) continue;
                        if (k < 0 ||
                            (k != j && compGT(file[k]->rec.key, file[j]->rec.key)))
                            k = j;
                    }
                    if (k < 0) break;

                    /* write record[k] to file[fileT] */
                    file[fileT]->f.write((char*)&file[k]->rec, sizeof(RecType));
                    if (file[fileT]->f.fail()) {
                        perror("io6");
                        cleanExit(1);
                    }

                    /* replace record[k] */
                    lastKey = file[k]->rec.key;
                    file[k]->f.read((char*)&file[k]->rec, sizeof(RecType));
                    if (!file[k]->f.fail()) {
                        /* check for end of run on file[s] */
                        if (compLT(file[k]->rec.key, lastKey))
                            file[k]->eor = true;
                    } else if (file[k]->f.eof()) {
                        file[k]->eof = true;
                        file[k]->eor = true;
                    } else {
                        perror("io7");
                        cleanExit(1);
                    }
                }

                /* fixup dummies */
                for (auto j = 0; j <= fileP; j++) {
                    if (file[j]->dummy) file[j]->dummy--;
                    if (!file[j]->eof) file[j]->eor = false;
                }

            } else if (allDummies) {
                for (auto j = 0; j <= fileP; j++)
                    file[j]->dummy--;
                file[fileT]->dummy++;
            }

            /* end of run */
            if (file[fileP]->eof && !file[fileP]->dummy) {
                /* completed a fibonocci-level */
                level--;
                if (!level) {
                    /* we're done, file[fileT] contains data */
                    return;
                }

                /* fileP is exhausted, reopen as new */
                file[fileP]->f.close();
                file[fileP]->f.open(file[fileP]->name, ios_base::out |ios_base::in | ios_base::trunc | ios_base::binary);
                if (file[fileP]->f.fail()) {
                    perror("io8");
                    cleanExit(1);
                }
                file[fileP]->eof = false;
                file[fileP]->eor = false;

                rewindFile(fileT);

                /* f[0],f[1]...,f[fileT] <-- f[fileT],f[0]...,f[T-1] */
                tfile = file[fileT];
                for (auto i = file.size() - 1; i > 0; i--) {
                    file[i] = file[i - 1];
                }
                file[0] = tfile;

                /* start new runs */
                for (auto j = 0; j <= fileP; j++)
                    if (!file[j]->eof) file[j]->eor = false;
            }
        }

    }
}

void ExternalSorter::extSort() {
    initTmpFiles();
    makeRuns();
    mergeSort();
    termTmpFiles(0);
}




