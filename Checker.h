#ifndef ISLAB_CHECKER_H
#define ISLAB_CHECKER_H

#include <string>

using std::string;

class Checker {
private:
    static const int REC_COUNT = 10000;
public:
    static void generateInputFile(const string& fileName);
    static bool checkOutputFile(const string& fileName);
};


#endif //ISLAB_CHECKER_H
