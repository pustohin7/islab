#include <cstdlib>
#include <ctime>
#include <fstream>
#include "Checker.h"
#include "RecType.h"

using std::ofstream;
using std::ifstream;
using std::ios_base;

void Checker::generateInputFile(const string &fileName) {
    ofstream inputFile;
    inputFile.open(fileName, ios_base::out | ios_base::trunc | ios_base::binary);

    srand((unsigned int) time(NULL));

    RecType newRec;
    for (auto i = 0; i < REC_COUNT; i++) {
        newRec.key = rand();
        inputFile.write((char*)&newRec, sizeof(RecType));
    }
    inputFile.close();
}

bool Checker::checkOutputFile(const string &fileName) {
    ifstream outputFile;
    outputFile.open(fileName, ios_base::in | ios_base::binary);

    RecType rec1, rec2;
    outputFile.read((char*)&rec1, sizeof(RecType));
    bool res = true;

    for (auto i = 1; i < REC_COUNT && res; i++) {
        outputFile.read((char*)&rec2, sizeof(RecType));
        res &= rec2.key >= rec1.key;
        rec1 = rec2;
    }
    outputFile.close();

    return res;
}



