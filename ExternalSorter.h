#ifndef ISLAB_EXTERNALSORTER_H
#define ISLAB_EXTERNALSORTER_H

#include <string>
#include <vector>
#include <fstream>
#include "RecType.h"

using std::string;
using std::vector;
using std::fstream;

class ExternalSorter {
private:
    struct TmpFileTag {
        fstream f;                  /* file pointer */
        string name;                /* filename */
        RecType rec;                /* last record read */
        int dummy;                  /* number of dummy runs */
        bool eof;                   /* end-of-file flag */
        bool eor;                   /* end-of-run flag */
        bool valid;                 /* true if rec is valid */
        int fib;                    /* ideal fibonacci number */
    };

    vector<TmpFileTag*> file;        /* array of file info for tmp files */

    unsigned int nTmpFiles;           /* number of tmp files */
    string ifName;            /* input filename */
    string ofName;            /* output filename */

    int level;               /* level of runs */
    unsigned int nNodes;

    void deleteTmpFiles();
    void termTmpFiles(int rc);
    void cleanExit(int rc);
    void initTmpFiles();
    RecType* readRec();
    void makeRuns();
    void rewindFile(int j);
    void mergeSort();

    template<typename T> bool compLT(T x, T y);
    template<typename T> bool compGT(T x, T y);

public:
    ExternalSorter(string &ifName, string &ofName, unsigned int nTmpFiles, unsigned int nNodes)
            : ifName(ifName), ofName(ofName), nTmpFiles(nTmpFiles), nNodes(nNodes) { }
    void extSort();
};


#endif //ISLAB_EXTERNALSORTER_H
