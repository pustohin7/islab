#ifndef ISLAB_RECTYPE_H
#define ISLAB_RECTYPE_H

typedef int keyType;

class RecType {
private:
    static const int LRECL = 100;
public:
    keyType key;
    char data[LRECL - sizeof(keyType)];
};


#endif //ISLAB_RECTYPE_H
