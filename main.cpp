#include <iostream>
#include <cstdlib>

#include "ExternalSorter.h"
#include "Checker.h"

using std::cout;
using std::endl;

int main(int argc, char *argv[]) {
    if (argc != 5) {
        cout << argv[0] << " ifName ofName nTmpFiles nNodes" << endl;
        return 1;
    }

    string ifName = argv[1];
    string ofName = argv[2];
    unsigned int nTmpFiles = (unsigned int) atoi(argv[3]);
    unsigned int nNodes = (unsigned int) atoi(argv[4]);

    cout << "extSort: nFiles=" << nTmpFiles << ", nNodes=" << nNodes <<
            ", lrecl=" << sizeof(RecType) << endl;

    ExternalSorter externalSorter { ifName, ofName, nTmpFiles, nNodes };
    Checker checker;

    checker.generateInputFile(ifName);
    externalSorter.extSort();

    bool check = checker.checkOutputFile(ofName);
    if (check) {
        cout << "Check OK" << endl;
    } else {
        cout << "Check ERROR" << endl;
    }

    return 0;
}